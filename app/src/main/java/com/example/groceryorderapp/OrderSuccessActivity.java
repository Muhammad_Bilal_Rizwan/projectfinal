package com.example.groceryorderapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.groceryorderapp.model.ShopModel;

public class OrderSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);

        ShopModel shopModel = getIntent().getParcelableExtra("ShopModel");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(shopModel.getName());
        actionBar.setSubtitle(shopModel.getAddress());
        actionBar.setDisplayHomeAsUpEnabled(false);

        TextView buttonDone = findViewById(R.id.buttonDone);
        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}